package noobbot;

import java.util.List;
import java.util.Map;

public final class Data {
	public String myCar = "";
	public boolean crashed = false;
	public int gameTick = 0;
	public int lastGameTick = 0;
	public int piece = 0;
	public double inPieceDistance = 0.0;
	public double angle = 0.0;

	public double turboDuration = 0.0;
	public int turboTicks = 0;
	public double turboFactor = 1.0;
	public boolean turboOn = false;

	public double[][] track;
	public boolean[] switchh;


	public void tick(int newTick, List carPositions) {
		for(Object o : carPositions) {
			Map m = (Map)o;
			if (s(m, "id", "color").equals(myCar)) {
				this.angle = d(m, "angle");
				this.piece = i(m, "piecePosition", "pieceIndex");
				this.inPieceDistance = i(m, "piecePosition", "inPieceDistance");
			}
		}
		int change = (newTick - lastGameTick);
		gameTick += change;
		if (turboOn) {
			turboTicks = java.lang.Math.max(turboTicks - change, 0);
			if (turboTicks == 0) {
				turboOn = false;
			}
		}
		lastGameTick = newTick;
	}

	public void turboAvailable(double dur, int ticks, double fact) {
		this.turboDuration = dur;
		this.turboTicks = ticks;
		this.turboFactor = fact;
	}

	public double straitLength(boolean semi) {
		int i = this.piece;
		double ret = 0.0;
		for(;;i = (i+1) % track.length) {
			double[] block = track[i];
			if (block[1] != 0.0) {
				//TODO: semi
				break;
			}
			ret += block[0];
		}
		return ret;
	}

	public boolean turboNow() {
		if (turboOn || turboTicks <= 0)
			return false;
		double d = straitLength(true);
		boolean ret = straitLength(true) > 150.0;
		if (ret) {
			turboOn = true;
			System.out.println("turbo "+d+ " at "+piece+"["+str(track[piece])+"]");
		}
		return ret;
	}

	public void setTrack( List list ) {
		this.track = new double[list.size()][];
		this.switchh = new boolean[list.size()];
		for(int i = 0; i < track.length; i++) {
			Map m = (Map)list.get(i);
			this.switchh[i] = (m.get("switch") != null);
			Object x = m.get("length"),
				 r = m.get("radius"),
				 a = m.get("angle");
			track[i] = new double[]{
				(x != null) ? d(x)  :  (d(r) * d(a) / 360.0 * java.lang.Math.PI * 2.0),
				(r != null ? d(r) : 0.0),
				(a != null ? d(a) : 0.0)
			};
		}
	}
	private double d(Object s) { return Double.parseDouble(s.toString()); }
	private double d(Map m, String key) { return d(m.get(key).toString()); }
	private String str(double[] a) { return java.util.Arrays.toString(a); }
	private Object o(Map m, String k, String k2) { return ((Map)m.get(k)).get(k2);}
	private String s(Map m, String k, String k2) { return o(m,k,k2).toString();	}
	private int i(Object s) { return new Double(s.toString()).intValue(); }
	private int i(Map m, String k, String k2) { return i(o(m,k,k2));	}

}