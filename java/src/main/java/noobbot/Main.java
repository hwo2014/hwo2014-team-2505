package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;

public class Main {
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new Join(botName, botKey));
    }

    final Gson gson = new Gson();
    private PrintWriter writer;

    Data data = new Data();

    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        String line = null;

        send(join);
        int i = 0,  j = 0;
        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            if (msgFromServer.msgType.equals("carPositions")) {
                if (i++ < 6)
                    printData(msgFromServer);
                data.tick(j++, (List)msgFromServer.data); //TODO from msg
                turbo();
                send(new Throttle(0.5));
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
            } else if (msgFromServer.msgType.equals("gameInit")) {
                printData(msgFromServer);
                data.setTrack( (List)m(msgFromServer, "race", "track", "pieces") );
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
            } else if (msgFromServer.msgType.equals("yourCar")) {
                if (data.myCar.isEmpty())
                    data.myCar = m(msgFromServer, "color").toString();
            } else if (msgFromServer.msgType.equals("crash")) {
                printData(msgFromServer);
                data.crashed = true;    // TODO myCar
            } else if (msgFromServer.msgType.equals("spawn")) {
                printData(msgFromServer);
                data.crashed = false;
                send(new Throttle(1.0));
            } else if (msgFromServer.msgType.equals("turboAvailable")) {
                printData(msgFromServer);
                Map m = (Map)msgFromServer.data;
                data.turboAvailable(d(m.get("turboDurationMilliseconds")),
                    i(m.get("turboDurationTicks")), i(m.get("turboFactor")));
            } else if (msgFromServer.msgType.equals("dnf")) {
                printData(msgFromServer);
            } else {
                printData(msgFromServer);
                send(new Ping());
            }
        }
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }

    private void printData(MsgWrapper msgFromServer) {
        String s = msgFromServer.data == null ? "" : msgFromServer.data.toString();
        System.out.println(data.gameTick +"\t"+msgFromServer.msgType + ": " +
                s.substring(0, java.lang.Math.min(1000, s.length())));
    }

    private double d(Object s) { return Double.parseDouble(s.toString()); }
    private int i(Object s) { return new Double(s.toString()).intValue(); }
    private Object m(MsgWrapper msgFromServer, String k) { return ((Map)msgFromServer.data).get(k);}
    private Object m(MsgWrapper msgFromServer, String k, String k2, String k3) {
        return ((Map)((Map)((Map)msgFromServer.data).get(k)).get(k2)).get(k3);
    }
    private void turbo() {
        if (data.turboNow()) {
            send(new Turbo());
        }
    }
}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}

class SwitchLane extends SendMsg {
    private final int dir;

    public SwitchLane(int dir) {
        this.dir = dir;
    }

    @Override
    protected Object msgData() {
        return dir < 0 ? "Left" : "Right";
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
}

class Turbo extends SendMsg {

    @Override
    protected Object msgData() {
        return "...WHIZ...";
    }

    @Override
    protected String msgType() {
        return "turbo";
    }
}
